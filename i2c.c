/*********************************************//*!@addtogroup file Files*//*@{*/
/*!
 *  *******************************************************************************
 *  *       $Id: i2c.c  2016-08-30 08:00:00Z jmcolagrossi $
 *  * $Revision: 1 $
 *  *     $Date: 2016-08-30 08:00:00 +0100 (Thu, 30 septembre. 2016) $
 *  *   $Author: jmcolagrossi§$
 *  *
 *  *******************************************************************************
 *  *
 *  * @project:      I2C Driver for pic 24fj256gb108 using interrupt
 *  * @customer:
 *  *
 *  * @origAuthor:   jmcolagrossi
 *  *
 *  * @brief:        i2c driver in Master mode for non blocking function.
 *  * @description:  give to the application an easy way to read and write date
 *  *                to I2C device. The driver will be a non blocking driver
 *  *                driven by interrupt
 *  */
/************************************************************************//*@}*/

/******************************************************************************/
/* includes (#include)                                                        */
/******************************************************************************/
#include "p24FJ256GB108.h"
#include "i2c.h"

/************* NO FURTHER INCLUDES AFTER INCLUDE OF MODULE HEADER *************/
/******************************************************************************/
/* Definition of structures (struc)                                           */
/*******************************//*!@addtogroup struc general structures*//*@{*/



/***************************************************************************************/
/*!   ts_i2c_flags_t
 *
 *    \struct TS_I2C_FLAGS_T
 *    @brief:  internal flags used
 */

typedef struct TS_I2C_FLAGS_T {
   uint8_t initialized:1;        //!< initialized, already opened or not
   uint8_t busy:1;               //!< busy, is the port already in use
   uint8_t rdNOTwr:1;            //!< rdNOTwr, read or write command
   uint8_t restart:1;            //!< restart, must send the restart cmd
   uint8_t free4:1;
   uint8_t free5:1;
   uint8_t free6:1;
   uint8_t free7:1;
}ts_i2c_flags_t;

/***************************************************************************************/
/*!   te_i2c_stm_t
 *
 *    \struct TE_I2C_STM_T
 *    @brief: interrupt state machine
 */
typedef enum TE_I2C_STM_T {

   I2C_SEND_DEV_ADD,
   I2C_SEND_CMD,
   I2C_SEND_RESTART,
   I2C_RW_DATA,
   I2C_STOP_SENT,

}te_i2c_stm_t;


/***************************************************************************************/
/*!   tagI2CCONBITS
 *
 *    \struct I2CCONBITS
 *    @brief: bits structure of the I2CxCON register
 */
// a copy of the struct in p24fjgb108.h  with a different name
typedef struct tagI2CCONBITS {
   unsigned SEN:1;
   unsigned RSEN:1;
   unsigned PEN:1;
   unsigned RCEN:1;
   unsigned ACKEN:1;
   unsigned ACKDT:1;
   unsigned STREN:1;
   unsigned GCEN:1;
   unsigned SMEN:1;
   unsigned DISSLW:1;
   unsigned A10M:1;
   unsigned IPMIEN:1;
   unsigned SCLREL:1;
   unsigned I2CSIDL:1;
   unsigned :1;
   unsigned I2CEN:1;
} I2CCONBITS;

/***************************************************************************************/
/*!   tagI2CSTATBITS
 *
 *    \struct I2CSTATBITS
 *    @brief: bits structure of the I2CxSTAT register
 */
// a copy of the struct in p24fjgb108.h with a different name
typedef struct tagI2CSTATBITS {
   union {
      struct {
         unsigned TBF:1;
         unsigned RBF:1;
         unsigned R_NOT_W:1;
         unsigned S:1;
         unsigned P:1;
         unsigned D_NOT_A:1;
         unsigned I2COV:1;
         unsigned IWCOL:1;
         unsigned ADD10:1;
         unsigned GCSTAT:1;
         unsigned BCL:1;
         unsigned :3;
         unsigned TRSTAT:1;
         unsigned ACKSTAT:1;
      };
      struct {
         unsigned :2;
         unsigned R_W:1;
         unsigned :2;
         unsigned D_A:1;
      };
   };
} I2CSTATBITS;

/***************************************************************************************/
/*!   I2CTRN array
 *
 *    \array I2CTRN
 *    @brief: pointer of the I2CxTRN register address
 */
volatile unsigned int * I2CTRN[I2C_LAST_ID] = {

      ((volatile unsigned int *)(&I2C1TRN)),
      ((volatile unsigned int *)(&I2C2TRN)),
      ((volatile unsigned int *)(&I2C3TRN)),

};

/***************************************************************************************/
/*!   I2CRCV array
 *
 *    \array I2CRCV
 *    @brief: pointer of the I2CxRCV register address
 */
volatile unsigned int * I2CRCV[I2C_LAST_ID] = {

      ((volatile unsigned int *)(&I2C1RCV)),
      ((volatile unsigned int *)(&I2C2RCV)),
      ((volatile unsigned int *)(&I2C3RCV)),
};

/***************************************************************************************/
/*!   inI2CCONBits array
 *
 *    \array inI2CCONBitsV
 *    @brief: pointer of the I2CxCON register
 */
// array of the address of the registers I2CxCON
volatile I2CCONBITS * inI2CCONbits[I2C_LAST_ID]  = {
   ((volatile I2CCONBITS *)(&I2C1CONbits)),
   ((volatile I2CCONBITS *)(&I2C2CONbits)),
   ((volatile I2CCONBITS *)(&I2C3CONbits)),
};

/***************************************************************************************/
/*!   inI2CSTATBits array
 *
 *    \array inI2CSTATBitsV
 *    @brief: pointer of the I2CxSTAT register
 */
// array of the address of the registers I2CSTAT
volatile I2CSTATBITS * inI2CSTATbits[I2C_LAST_ID] = {
   ((volatile I2CSTATBITS *)(&I2C1STATbits)),
   ((volatile I2CSTATBITS *)(&I2C2STATbits)),
   ((volatile I2CSTATBITS *)(&I2C3STATbits)),
};

typedef struct TS_I2C_DEV_T {
   te_i2c_id_t       id;
   ts_i2c_flags_t    flags;
   te_i2c_stm_t      i2c_st_m;
   ts_i2c_param_t *  param;
   ts_i2c_data_t  *  data;
}ts_i2c_dev_t;

ts_i2c_dev_t   glb_i2c_dev[I2C_LAST_ID];

typedef struct TS_INDATA_TO_SEND_T {
   uint8_t nbData_rw;
   uint8_t * pData_rw;
}ts_data_rw_t;

ts_data_rw_t data_rw[I2C_LAST_ID];

/***** End of: struct general structures *********************************//*@}*/

/******************************************************************************/
/* Instantiation of maros (macros)                     */
/*******************************//*!@addtogroup macros general macros*//*@{*/

#define _i2c_set_initialized(c)  (glb_i2c_dev[c].flags.initialized = 1)
#define _i2c_clr_initialized(c)  (glb_i2c_dev[c].flags.initialized = 0)
#define _is_i2c_initialized(c)   (glb_i2c_dev[c].flags.initialized == 1? 1: 0)

#define _i2c_set_busy(c)         (glb_i2c_dev[c].flags.busy = 1)
#define _i2c_clr_busy(c)         (glb_i2c_dev[c].flags.busy = 0)
#define _is_i2c_busy(c)          (glb_i2c_dev[c].flags.busy == 1? 1: 0)

#define _i2c_set_read(c)         (glb_i2c_dev[c].flags.rdNOTwr = 1)
#define _i2c_clr_read(c)         (glb_i2c_dev[c].flags.rdNOTwr = 0)
#define _is_i2c_read(c)          (glb_i2c_dev[c].flags.rdNOTwr == 1? 1: 0)
#define _i2c_set_write(c)        (glb_i2c_dev[c].flags.rdNOTwr = 0)
#define _i2c_clr_write(c)        (glb_i2c_dev[c].flags.rdNOTwr = 1)
#define _is_i2c_write(c)         (glb_i2c_dev[c].flags.rdNOTwr == 0? 1: 0)

#define _is_i2c_restart_sent(c)  (glb_i2c_dev[c].flags.restart == 1? 1: 0)
#define _i2c_set_restart(c)      (glb_i2c_dev[c].flags.restart = 1)
#define _i2c_clr_restart(c)      (glb_i2c_dev[c].flags.restart = 0)


#define _set_nack(c)             do {                                \
                                       inI2CCONbits[c]->ACKDT = 1;   \
                                       inI2CCONbits[c]->ACKEN = 1;   \
                                 }while (0)

#define _set_ack(c)              do {                                \
                                       inI2CCONbits[c]->ACKDT = 0;   \
                                       inI2CCONbits[c]->ACKEN = 1;   \
                                 }while (0)

#define _set_stop(c)             do {                                \
                                       inI2CCONbits[c]->PEN = 1;     \
                                 }while (0)


#define _set_start(c)            do {                                \
                                       inI2CCONbits[c]->SEN = 1;     \
                                 }while (0)

#define _set_restart(c)          do {                                \
                                       inI2CCONbits[c]->RSEN = 1;    \
                                 }while (0)

#define _clr_ov(c)              do {                                 \
                                      inI2CSTATbits[c]->I2COV = 0;   \
                                }while (0)



#define i2c_send_start(c)     do {                                   \
                                    _clr_ov(c);                      \
                                    _i2c_clr_restart(c);             \
                                    _set_start(c);                   \
                              }while (0)


/***** End of: macros general macros    *********************************//*@}*/

/******************************************************************************/
/* Instantiation of global variables or constants (const)                     */
/*******************************//*!@addtogroup globvar Global Variables*//*@{*/



/***** End of: globvar Global variables *********************************//*@}*/

/******************************************************************************/
/* Local Functions                                                            */
/********************************//*!@addtogroup locfunc Local functions*//*@{*/

/*******************************************************************************
 *******************************************************************************/


inline void i2c_send_dev_add (te_i2c_id_t id)
{

   uint16_t val;
   static uint8_t st[I2C_LAST_ID] = {0,0,0};

   val = glb_i2c_dev[id].data->dev_add;

   switch (st[id]) {
      case 0:
         if (glb_i2c_dev[id].param->i2c_add_nb_bits == FR_10BITS_ADD) {
            // send high byte
            val = val >> 7; // bit A9 -> A2'
            val |= 0xf0;
            // For a 10 bits address devices
            // add is : 1 1 1 1 0 A9 A8 r/w for the high payload
            // add is A7 A6 A5 A4 A3 A2 A1 A0 for the low payload
            val &= 0b11110110;      //write
            st[id] ++;

            if (_is_i2c_restart_sent(id)) {
               val |= 0b00000001;   //read
               glb_i2c_dev[id].i2c_st_m = I2C_RW_DATA;
               st[id] = 0;
            }
         }
         else {

            // For a 7 bits address devices
            // val is  A6 A5 A4 A3 A2 A1 A0 r/w

            val = val << 1;
            val &= 0b11111110;      // write
            glb_i2c_dev[id].i2c_st_m = I2C_SEND_CMD;

            if (_is_i2c_restart_sent(id)) {
               val |= 0b00000001;   //read
               glb_i2c_dev[id].i2c_st_m = I2C_RW_DATA;
            }
         }
         break;

      case 1:
            // For a 10 bits address devices
            // val is  A7 A6 A5 A4 A3 A2 A1 A0
         val = val & 0x00FF;
         glb_i2c_dev[id].i2c_st_m = I2C_SEND_CMD;
         st[id] = 0;
         break;
   }

   if (glb_i2c_dev[id].param->i2c_add_nb_bits == FR_10BITS_ADD) {
      if (st[id] == 0) {
         //second byte dev add to send
         if (inI2CSTATbits[id]->ACKSTAT == 0) {
            //check the ACK value for the slave device
            //if ok, send next addre
            *I2CTRN[id] = (uint8_t)val;
         }
         else {

            _set_stop(id);
            // give to the application the possibility
            // to react
            if (glb_i2c_dev[id].data->i2c_error_cb != NULL){
               glb_i2c_dev[id].data->i2c_error_cb();

            }
         }
      }
      else {
         *I2CTRN[id] = (uint8_t)val;

      }
   }
   else {
      *I2CTRN[id] = (uint8_t)val;
   }

}

/*************************************************************************************
 *
 * i2c_send_cmd
 */
inline void i2c_send_cmd (te_i2c_id_t id)
{
   static uint8_t st[I2C_LAST_ID] = {0,0,0};
   uint16_t val;

   val = glb_i2c_dev[id].data->dev_cmd;

   switch (st[id]) {
      case 0:
         if (glb_i2c_dev[id].data->i2c_cmd_nb_bytes ==  I2C_CMD_2_BYTES) {
            val = val >> 8;
            st[id] ++;
         }
         else {
            val = val & 0x00ff;
            if (_is_i2c_read(id)) {
               // if a read must be done -> restart must be sent
               glb_i2c_dev[id].i2c_st_m = I2C_SEND_RESTART;
            }
            else {
               glb_i2c_dev[id].i2c_st_m = I2C_RW_DATA;
            }

         }
         break;
      case 1:
         val = val & 0x00ff;
         if (_is_i2c_read(id)) {

            glb_i2c_dev[id].i2c_st_m = I2C_SEND_RESTART;
         }
         else {
            glb_i2c_dev[id].i2c_st_m = I2C_RW_DATA;
         }

         st[id] = 0;
         break;
      default:
         st[id] = 0;

   }


   if (inI2CSTATbits[id]->ACKSTAT == 0) {
      //check the ACK value from the slave device
      //if ok, send next addre
      *I2CTRN[id] = (uint8_t)val;
   }
   else {
      // error
      _set_stop(id);
      // give to the application the possibility
      // to react
      if (glb_i2c_dev[id].data->i2c_error_cb != NULL){
         glb_i2c_dev[id].data->i2c_error_cb();

      }

   }

}


/*************************************************************************************
 *
 * i2c_send_restart
 */
inline void i2c_send_restart (te_i2c_id_t id)
{

   _i2c_set_restart(id);
   glb_i2c_dev[id].i2c_st_m = I2C_SEND_DEV_ADD;
   _set_restart(id);

}

/*************************************************************************************
 *
 * i2c_send_rw_data
 */
inline void i2c_rw_data (te_i2c_id_t id)
{


   if (_is_i2c_read(id)) {
      // read the data
      if (inI2CSTATbits[id]->RBF) {
         // data in buffer
         *data_rw[id].pData_rw = *I2CRCV[id];

         // the first time we know that nb_Data > 0
         data_rw[id].nbData_rw --;
         if (data_rw[id].nbData_rw) {
            // is not the last one -> we set ack
            data_rw[id].pData_rw ++;
            _set_ack(id);
         }
         else {
            // it is the last one -> we set nack
            _set_nack(id);
         }
      }
      else {
         // after a ack or nack interrupt must pass here
         if (data_rw[id].nbData_rw) {
            // more data to read
            inI2CCONbits[id]->RCEN = 1;
         }
         else {
            // no more data to read, send stop
            _set_stop(id);
            glb_i2c_dev[id].i2c_st_m = I2C_STOP_SENT;
         }

      }
   }
   else {
      //write data
      if (inI2CSTATbits[id]->ACKSTAT == 0) {
         // data acknowledged
         if (data_rw[id].nbData_rw) {

            *I2CTRN[id] = *data_rw[id].pData_rw;
            data_rw[id].nbData_rw --;
            if (data_rw[id].nbData_rw) {
               // inc pointer only if new data must be sent
               data_rw[id].pData_rw ++;
            }
         }
         else {
            _set_stop(id);
            glb_i2c_dev[id].i2c_st_m = I2C_STOP_SENT;
         }

      }
      else {
         _set_stop(id);
         // give to the application the possibility
         // to react
         if (glb_i2c_dev[id].data->i2c_error_cb != NULL){
            glb_i2c_dev[id].data->i2c_error_cb();
         }
      }

   }

}

/*************************************************************************************
 *
 * i2c_stop_sent
 */
inline void i2c_stop_sent (te_i2c_id_t id)
{

   // call the callback to notify the application that
   // the transfert is done

   if (glb_i2c_dev[id].data->i2c_endInt_cb != NULL) {
      glb_i2c_dev[id].data->i2c_endInt_cb();
   }
   _i2c_clr_busy(id);
}

/*************************************************************************************
 *
 * _isr_error_process FIXME: must be redesigned
 */
inline void _isr_error_process(te_i2c_id_t id)
{
   // give to the application the possibility
   // to react
   if (glb_i2c_dev[id].data->i2c_error_cb != NULL){
      glb_i2c_dev[id].data->i2c_error_cb();

   }
   glb_i2c_dev[id].i2c_st_m = I2C_STOP_SENT;

}

/*************************************************************************************
 *
 * _isr_data_process
 */
inline void _isr_data_process(te_i2c_id_t id)
{
   switch (glb_i2c_dev[id].i2c_st_m){
      case I2C_SEND_DEV_ADD:
         // interrupt start bit sent !
         // Send the Device address (7 or 10 bits with write)
         // The master must wait for an ack on each byte
         i2c_send_dev_add(id);
         break;
      case I2C_SEND_CMD:
         // interrupt on ack!
         // Send the command to the device
         // The master must wait for an ack on each byte
         i2c_send_cmd (id);
         break;
      case I2C_SEND_RESTART:
         // interrupt on ack!
         // if a read is in process, a resend start must be processed
         // The master must wait for an ack on each byte
         i2c_send_restart(id);
         break;
      case I2C_RW_DATA:
         i2c_rw_data(id);
         break;
      case I2C_STOP_SENT:
         i2c_stop_sent (id);
         break;
      default:
         break;
   }
}


/********************************************************************************/
/*! master i2c  driver interrupt
 *
 */
#define _BCL      (10)
#define _IWCOL    (7)
#define _I2COV    (6)

/*********************************************************************************
 *
 * Interrupt I2C port 1
 *
 */
void __attribute__((interrupt,auto_psv)) _MI2C1Interrupt (void)
{

   // _BCL: Master Bus Collision Detect bit
   // _IWCOL: Write Collision Detect bit
   // _I2COV: Receive Overflow Flag bit

   if (I2C1STAT & (_BV(_BCL) |_BV(_IWCOL) | _BV(_I2COV))) {
      glb_i2c_dev[I2C_1].data->err_val = I2C1STAT & (_BV(_BCL) | _BV(_IWCOL) | _BV(_I2COV));

      _isr_error_process(I2C_1);
      I2C1STAT &= ~(_BV(_BCL) | _BV(_IWCOL) | _BV(_I2COV));
      _set_stop(I2C_1);

   }
   else {
      _isr_data_process(I2C_1);
   }

   _MI2C1IF = 0;
}

/*********************************************************************************
 *
 * Interrupt I2C port 2
 *
 */
void __attribute__((interrupt,auto_psv)) _MI2C2Interrupt (void)
{

   if (I2C2STAT & (_BV(_BCL) | _BV(_IWCOL) | _BV(_I2COV))) {
      glb_i2c_dev[I2C_2].data->err_val = I2C2STAT & (_BV(_BCL) | _BV(_IWCOL) | _BV(_I2COV));

      _set_stop(I2C_2);
      _isr_error_process(I2C_2);

      I2C2STAT &= ~(_BV(_BCL) | _BV(_IWCOL) | _BV(_I2COV));

   }
   else {
      _isr_data_process(I2C_2);
   }
   _MI2C2IF = 0;

}

/*********************************************************************************
 *
 * Interrupt I2C port 3
 *
 */
void __attribute__((interrupt,auto_psv)) _MI2C3Interrupt (void)
{

   if (I2C3STAT & (_BV(_BCL) |_BV(_IWCOL) | _BV(_I2COV))) {
      glb_i2c_dev[I2C_3].data->err_val = I2C3STAT & (_BV(_BCL) | _BV(_IWCOL) | _BV(_I2COV));
      ;

      _set_stop(I2C_3);
      _isr_error_process(I2C_3);
      I2C3STAT &= ~(_BV(_BCL) | _BV(_IWCOL) | _BV(_I2COV));
   }
   else {

      _isr_data_process(I2C_3);
   }

   _MI2C3IF = 0;
}

/***** End of: locfunc Local functions  *********************************//*@}*/

/******************************************************************************/
/* Global Functions                                                            */
/*******************************//*!@addtogroup globfunc Global functions*//*@{*/

/*******************************************************************************
 *******************************************************************************/

/*********************************************************************************
 *
 * Give the information if the port is already opened or not
 *
 */
uint8_t i2c_is_initialized(te_i2c_id_t id)
{
   return (_is_i2c_initialized(id));

}

/*********************************************************************************
 *
 * Give the information if the port is busy or not
 *
 */
uint8_t i2c_is_busy(te_i2c_id_t id)
{
   return (_is_i2c_busy(id));

}


/********************************************************************************
 ********************************************************************************
 **  FSCL = FCY /(I2CxBRG + 1 + (FCY / 10 000 000))
 **
 **  or I2CxBRG = ((FCY / FSCL) - (FCY / 10 000 000)) - 1
 **
 ** Note 1: Based on FCY = FOSC/2; Doze mode and
 ** PLL are disabled.
 ** 2: These clock rate values are for guidance
 ** only. The actual clock rate can be affected
 ** by various system level parameters. The
 ** actual clock rate should be measured in
 ** its intended application.
 **
 **
 **  -----------------------------------  I2CxBRG Value -------------------------
 ** |Required System FSCL |  FCY     | (Decimal) | (Hexadecimal) | Actual FSCL   |
 ** |---------------------|----------|-----------|---------------|---------------|
 ** |      100 kHz        |  16 MHz  |   157     |        9D     |     100 kHz   |
 ** |      100 kHz        |   8 MHz  |    78     |        4E     |     100 kHz   |
 ** |      100 kHz        |   4 MHz  |    39     |        27     |      99 kHz   |
 ** |      400 kHz        |  16 MHz  |    37     |        25     |     404 kHz   |
 ** |      400 kHz        |   8 MHz  |    18     |        12     |     404 kHz   |
 ** |      400 kHz        |   4 MHz  |     9     |         9     |     385 kHz   |
 ** |      400 kHz        |   2 MHz  |     4     |         4     |     385 kHz   |
 ** |        1 MHz        |  16 MHz  |    13     |         D     |     1.026 MHz |
 ** |        1 MHz        |   8 MHz  |     6     |         6     |     1.026 MHz |
 ** |        1 MHz        |   4 MHz  |     3     |         3     |     0.909 MHz |
 ** |----------------------------------------------------------------------------|
 **
 **
 *********************************************************************************/
/*********************************************************************************
 *
 * Open the I2C port
 *
 */

uint8_t i2c_open (te_i2c_id_t id, ts_i2c_param_t * param)
{

   // check the validity of all the parameters

   if (id >= I2C_LAST_ID) {
      return (0);
   }

   if (param == NULL) {
      return (0);
   }

   if (param->i2c_add_nb_bits > FR_10BITS_ADD) {
      return (0);
   }


   if (param->i2c_bus_freq > FREQ_1MHZ) {
      return (0);
   }


   if(_is_i2c_initialized(id)) {
      // i2c already  opened
      return (0);
   }

   switch( id )
   {
      case I2C_1:
         I2C1CON = 0;
         I2C1CONbits.I2CEN = 1;   // Enable I2C
         if (param->i2c_bus_freq == FREQ_100kHZ) {
            I2C1BRG = 157;
         }
         else if (param->i2c_bus_freq == FREQ_400kHZ) {

            I2C1BRG = 37;         // 400 kHz @ 16 MHz
         }
         else if (param->i2c_bus_freq == FREQ_1MHZ) {
            I2C1BRG = 13;         // 400 kHz @ 16 MHz
         }
         _MI2C1P = 4;
         _MI2C1IF = 0;
         _MI2C1IE = 1;
         _i2c_set_initialized(id);
         break;
      case I2C_2:
         I2C2CON = 0;
         I2C2CONbits.I2CEN = 1;   // Enable I2C
         if (param->i2c_bus_freq == FREQ_100kHZ) {
            I2C2BRG = 157;
         }
         else if (param->i2c_bus_freq == FREQ_400kHZ) {

            I2C2BRG = 37;         // 400 kHz @ 16 MHz
         }
         else if (param->i2c_bus_freq == FREQ_1MHZ) {
            I2C2BRG = 13;         // 400 kHz @ 16 MHz
         }
         _MI2C2P = 4;
         _MI2C2IF = 0;
         _MI2C2IE = 1;
         _i2c_set_initialized(id);
         break;
      case I2C_3:
         I2C3CON = 0;
         I2C3CONbits.I2CEN = 1;   // Enable I2C
         if (param->i2c_bus_freq == FREQ_100kHZ) {
            I2C3BRG = 157;
         }
         else if (param->i2c_bus_freq == FREQ_400kHZ) {

            I2C3BRG = 37;         // 400 kHz @ 16 MHz
         }
         else if (param->i2c_bus_freq == FREQ_1MHZ) {
            I2C3BRG = 13;         // 400 kHz @ 16 MHz
         }
         _MI2C3P = 4;
         _MI2C3IF = 0;
         _MI2C3IE = 1;
         _i2c_set_initialized(id);
         break;
      default:
         break;
   }

   glb_i2c_dev[id].param = param;
   return (1);
}




/*********************************************************************************
 *
 * Close the I2C port
 *
 */

uint8_t i2c_close (te_i2c_id_t id)
{

   glb_i2c_dev[id].flags.initialized = 0;
   glb_i2c_dev[id].flags.busy = 0;
   glb_i2c_dev[id].flags.rdNOTwr = 0;
   glb_i2c_dev[id].flags.restart = 0;

   switch (id) {
      case I2C_1:
         _MI2C1IF = 0;
         _MI2C1IE = 0;
         _MI2C1P = 0;
         break;
      case I2C_2:
         _MI2C2IF = 0;
         _MI2C2IE = 0;
         _MI2C2P = 0;
         break;
      case I2C_3:
         _MI2C3IF = 0;
         _MI2C3IE = 0;
         _MI2C3P = 0;
         break;
      default:
         break;

   }
   return (1);
}

/*********************************************************************************
 *
 * receive data into data struct to the I2C port
 *
 */
uint8_t i2c_read(te_i2c_id_t id, ts_i2c_data_t * data)
{
   if(!_is_i2c_initialized(id)) {
      // i2c is not open
      return (0);
   }

   if (data == NULL) {
      return (0);
   }

   if (data->nb_Data == 0) {
      return (0);
   }

   // block the system -> the application must be sure that
   // i2c is not busy
   while (_is_i2c_busy(id));

   glb_i2c_dev[id].flags.busy = 1;
   data_rw[id].nbData_rw = data->nb_Data;
   data_rw[id].pData_rw = data->data;

   glb_i2c_dev[id].id = id;
   glb_i2c_dev[id].data = data;
//   glb_i2c_dev[id].data.err_val = 0;
   glb_i2c_dev[id].i2c_st_m = I2C_SEND_DEV_ADD;

   glb_i2c_dev[id].flags.rdNOTwr = 1;

   i2c_send_start(id);
   return (1);
}

/*********************************************************************************
 *
 * send data into data struct to the I2C port
 *
 */
uint8_t i2c_write(te_i2c_id_t id, ts_i2c_data_t * data)
{
   if(!_is_i2c_initialized(id)) {
      // i2c is not open
      return (0);
   }

   if (data == NULL) {
      return (0);
   }

   if (data->nb_Data == 0) {
      return (0);
   }

   // block the system -> the application must be sure that
   // i2c is not busy
   while (_is_i2c_busy(id));
   glb_i2c_dev[id].flags.busy = 1;

   data_rw[id].nbData_rw = data->nb_Data;
   data_rw[id].pData_rw = data->data;

   glb_i2c_dev[id].id = id;
   glb_i2c_dev[id].data = data;
//   glb_i2c_dev[id].data.err_val = 0;
   glb_i2c_dev[id].i2c_st_m = I2C_SEND_DEV_ADD;

   glb_i2c_dev[id].flags.rdNOTwr = 0;

   i2c_send_start(id);
   return (1);

}
/***** End of: globfunc global functions *********************************//*@}*/

