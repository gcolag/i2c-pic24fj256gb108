/*********************************************//*!@addtogroup file Files*//*@{*/
/*!
 *  *******************************************************************************
 *  *       $Id: i2c.c  2016-08-30 08:00:00Z jmcolagrossi $
 *  * $Revision: 1 $
 *  *     $Date: 2016-08-30 08:00:00 +0100 (Thu, 30 septembre. 2016) $
 *  *   $Author: jmcolagrossi§$
 *  *
 *  *******************************************************************************
 *  *
 *  * @project:      I2C Driver for pic 24fj256gb108 using interrupt
 *  * @customer:
 *  *
 *  * @origAuthor:   jmcolagrossi
 *  *
 *  * @brief:        i2c driver in Master mode for non blocking function.
 *  * @description:  give to the application an easy way to read and write date
 *  *                to I2C device. The driver will be a non blocking driver
 *  *                driven by interrupt
 *  */
/************************************************************************//*@}*/

#ifndef  __I2C_H
#define   __I2C_H


/*********************************************************************************
 *
 * Address length of the device
 *
 */
typedef enum TE_ADD_LENGTH_T {
   FR_7BITS_ADD,
   FR_10BITS_ADD,
}te_i2c_add_length_t;

/*********************************************************************************
 *
 * Length of the command, actually limited to 2 bytes
 *
 */
typedef enum TE_I2C_CMD_LENGHT {
   I2C_CMD_1_BYTES,              // 8 bits cmd wide
   I2C_CMD_2_BYTES,              // 16 bits cmd wide (ex: EEPROM)
}te_i2c_cmd_length;

/*********************************************************************************
 *
 * Clk frequency of the i2c bus
 * WARNING, the code is based on 96MHz pll frequency
 *
 */
typedef enum TE_I2C_BUS_FREQ_T {
   FREQ_100kHZ,
   FREQ_400kHZ,
   FREQ_1MHZ,
}te_i2c_bus_freq_t;



/*********************************************************************************
 *
 * port ID
 * WARNING. The port must be already configured in output mode
 *
 */
typedef enum TE_I2C_ID_T
{
   I2C_1 = 0   ,
   I2C_2       ,
   I2C_3       ,
   I2C_LAST_ID ,
} te_i2c_id_t;

/*********************************************************************************
 *
 * data structure to send
 *
 */
typedef struct TS_DATA_T {
   uint16_t                   dev_add;             // 7 or 10 bits add
   // 7 bits or 10 bits
   uint16_t                   dev_cmd;             // cmd to send to the device (Max 2 bytes )
   te_i2c_cmd_length          i2c_cmd_nb_bytes;
   // i2c Clock frequency
   uint8_t *                  data;                // data to send or receive
   uint8_t                    nb_Data;             // nb data to send or receive
   uint16_t                   err_val;             // masq bit for BCL, IWCOL, I2COV error during R/W
   // give the application the possibility to react on error
   PFV                        i2c_error_cb;        // cb of for Application error managment
   PFV                        i2c_endInt_cb;
}ts_i2c_data_t;

/*********************************************************************************
 *
 * parameter structure to configure the i2c port
 *
 */
typedef struct TS_I2C_PARAM_T {
   // length of the command ( 2 bytes limited)
   te_i2c_add_length_t        i2c_add_nb_bits;
   te_i2c_bus_freq_t          i2c_bus_freq;
}ts_i2c_param_t;

void i2c_init(void);
uint8_t i2c_open(te_i2c_id_t id, ts_i2c_param_t * param);
uint8_t i2c_read(te_i2c_id_t id, ts_i2c_data_t * data);
uint8_t i2c_write(te_i2c_id_t id, ts_i2c_data_t * data);
uint8_t i2c_is_initialized(te_i2c_id_t id);
uint8_t i2c_is_busy(te_i2c_id_t id);
uint8_t i2c_close( te_i2c_id_t id );



#endif   /* I2C_H */


